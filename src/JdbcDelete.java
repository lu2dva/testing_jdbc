import javassist.bytecode.SourceFileAttribute;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcDelete {

    public static void main(String[] args) throws SQLException {

        Connection myConnection = null;
        try {
            // get a connection
            myConnection = DbConnection.getConnection("lu2dva", "pass", "false");
            System.out.println("Connected");
            // create a statement
            Statement myStmt = myConnection.createStatement();
            String sql = "DELETE from teacher where last_name='bukovsky'";

            // execute a querry
            int rowsAffected =  myStmt.executeUpdate(sql);


            System.out.println("Rows affected " + rowsAffected);
            System.out.println("Delete success");


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(myConnection != null) {
                myConnection.close();
            }

        }
    }
}
