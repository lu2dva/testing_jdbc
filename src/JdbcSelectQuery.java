import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcSelectQuery {

    public static void main(String[] args) throws SQLException {

        Connection myConnection = null;
        try {
            myConnection = DbConnection.getConnection("lu2dva", "pass", "false");
            System.out.println("Connected");

            Statement myStmt = myConnection.createStatement();
            ResultSet myRs = myStmt.executeQuery("SELECT * from students");

            while(myRs.next()) {
                System.out.println(myRs.getString("first_name") + ", " + myRs.getString("last_name"));
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(myConnection != null) {
                myConnection.close();
            }

        }
    }
}
