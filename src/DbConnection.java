import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnection {

    public static final String CONN = "jdbc:mysql://localhost/School";


    public static Connection getConnection(String user, String password, String useSSL) throws SQLException {
        Properties properties = new Properties();
        properties.setProperty("user", user);
        properties.setProperty("password", password);
        properties.setProperty("useSSL", useSSL);
        return DriverManager.getConnection(CONN,properties);
    }

}
