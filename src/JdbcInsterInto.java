import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcInsterInto {

    public static void main(String[] args) throws SQLException {

        Connection myConnection = null;
        try {
            // get a connection
            myConnection = DbConnection.getConnection("lu2dva", "pass", "false");
            System.out.println("Connected");
            // create a statement
            Statement myStmt = myConnection.createStatement();
            String sql = "INSERT INTO teacher" +
                    "(id, first_name, last_name, subject)" +
                    "VALUES ('905', 'Ludek', 'Bukovsky', 'Informatics')";

            // execute a querry
            myStmt.executeUpdate(sql);

            System.out.println("Insert success");


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(myConnection != null) {
                myConnection.close();
            }

        }
    }
}
