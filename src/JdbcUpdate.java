import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcUpdate {

    public static void main(String[] args) throws SQLException {

        Connection myConnection = null;
        try {
            // get a connection
            myConnection = DbConnection.getConnection("lu2dva", "pass", "false");
            System.out.println("Connected");
            // create a statement
            Statement myStmt = myConnection.createStatement();
            String sql = "UPDATE teacher set subject='Business' where id=905";

            // execute a querry
            myStmt.executeUpdate(sql);

            System.out.println("Update success");


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(myConnection != null) {
                myConnection.close();
            }

        }
    }
}
